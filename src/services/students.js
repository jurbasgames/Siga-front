import axios from "axios";

const api_url = "http://localhost:8080";
export async function getAllStudents() {
  const response = await axios.get(`${api_url}/aluno`);
  return response.data;
}
export async function deleteStudentByMatricula(matricula) {
  const response = await axios.delete(`${api_url}/aluno/` + matricula);
  return response.data;
}
export async function createStudent(data) {
  return axios.post(`${api_url}/aluno/novo`, data, {
    headers: { "Content-Type": "application/x-www-form-urlencoded" },
  });
}
export async function updateStudentByMatricula(matriculaAtual, data) {
  const response = await axios.put(`${api_url}/aluno/${matriculaAtual}`, data, {
    headers: { "Content-Type": "application/x-www-form-urlencoded" },
  });
  return response.data;
}
export async function cancelarMatricula(matricula) {
  const response = await axios.put(
    `${api_url}/aluno/cancelarMatricula/${matricula}`
  );
  return response.data;
}
